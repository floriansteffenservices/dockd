package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"gopkg.in/yaml.v2"

	"github.com/codegangsta/cli"
)

const (
	dockdHome   = "$HOME/.dockd"          // dockd user files
	imagesDir   = dockdHome + "/images"   // user image templates
	contextsDir = dockdHome + "/contexts" // user context files
	dockdEnvDir = ".dockd"                // dockd files in dev env
)

// holds minimum information about a docker-compose service
type service struct {
	name       string
	build      string
	dockerfile string
}

// data passed to the templates
type tmplData struct {
	Repo     string
	Tag      string
	FullName string
}

// template function, mapped to copy. It emits a COPY but from the
// .dockd asset directory.
func copyAsset(src, dst string) string {
	return fmt.Sprintf("COPY .dockd/%s %s", src, dst)
}

// template function, mapped to append. Like the copy function but
// the content of the asset file is appended to the dst file.
func appendAsset(src, dst string) string {
	return fmt.Sprintf(`COPY .dockd/%s /tmp/tmp-append
RUN cat /tmp/tmp-append >> %s && rm /tmp/tmp-append`, src, dst)
}

// parse the templates for the docker images stored in the user dir
func parseImageTemplates() *template.Template {
	glob := path.Join(os.ExpandEnv(imagesDir), "*")
	templates := template.New("/")
	// attach our custom functions
	funcs := template.FuncMap{"copy": copyAsset, "append": appendAsset}
	templates.Funcs(funcs)
	// parse the user templates
	_, err := templates.ParseGlob(glob)
	if err != nil {
		if strings.Contains(err.Error(), "pattern matches no files") {
			log.Fatalf("There is no image templates defined in %s.\nSee the help for more information.\n", glob)
		}
		log.Fatalf("Can't parse docker image templates. %s\n", err)
	}
	return templates
}

// exec the given command with the given arguments, and connect
// all standard io to the standard io of the current process.
func execPassthrough(name string, env []string, args ...string) {
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Env = env
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Can't execute %s. %s\n", name, err)
	}
}

// exec the given command with the given arguments, and capture the
// ouput and error in a buffer. The buffer is returned.
func execCapture(name string, env []string, args ...string) string {
	var buffer bytes.Buffer
	cmd := exec.Command(name, args...)
	cmd.Stdout = &buffer
	cmd.Stderr = &buffer
	cmd.Env = env
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Can't execute %s. %s\n", name, err)
	}
	return buffer.String()
}

// start docker machine if it is not already running
func startDockerMachine(name string) {
	machineStatus := execCapture("docker-machine", nil, "status", name)
	if !strings.HasPrefix(machineStatus, "Running") {
		fmt.Println("Starting docker-machine...")
		execPassthrough("docker-machine", nil, "start", name)
	}
}

// parse a docker-compose yaml configuration file. It returns the name of the
// project (stored in a YAML comment, dir name otherwise), the parsed file
// as a map of map and a list of services defined in the file. Only local
// services defined through a Dockerfile are returned.
func parseComposeConf(confName string) (proj string, conf map[string]map[string]interface{}, services []service) {

	// parse the compose yml file
	bytes, err := ioutil.ReadFile(confName)
	if err != nil {
		log.Fatalf("Can't read %s. %s\n", confName, err)
	}
	conf = make(map[string]map[string]interface{})
	err = yaml.Unmarshal(bytes, &conf)
	if err != nil {
		log.Fatalf("Can't parse %s. %s\n", confName, err)
	}

	// find the project name. It can be defined with a comment of the form
	// # project: name in the conf file, otherwise the name of the dir
	// holding the conf file is used.   var projectName string
	re := regexp.MustCompile(`(?mi)^#\s*project:\s*(\w+)`)
	matches := re.FindSubmatch(bytes)
	if len(matches) == 2 {
		proj = string(matches[1])
	} else {
		abspath, err := filepath.Abs(path.Dir(confName))
		if err != nil {
			log.Fatalf("Can't read project name. %s\n", err)
		}
		proj = path.Base(abspath)
	}

	// looking for services built from a Dockerfile
	services = make([]service, 0, 8)
	for name, config := range conf {
		build := config["build"]
		if build != nil {
			dockerfile := config["dockerfile"]
			if dockerfile == nil {
				dockerfile = "Dockerfile"
			}
			services = append(services, service{
				name:       name,
				build:      build.(string),
				dockerfile: dockerfile.(string),
			})
		}
	}

	return
}

type genStatus int

const (
	generated genStatus = iota
	uptodate
	unknownImage
)

// copies the dockd context files to the given build dir.
func copyContextFiles(serv service, force bool) {
	srcDir := os.ExpandEnv(contextsDir)
	dstDir := path.Join(serv.build, dockdEnvDir)
	contextUpdated := false
	// copy the given file to the build dir
	copyFile := func(srcPath string, srcInfo os.FileInfo, err error) error {
		if err != nil {
			log.Fatalf("Can't copy context file %s. %s\n", srcPath, err)
		}

		dst := filepath.Join(dstDir, strings.TrimPrefix(srcPath, srcDir))

		// if the file is a directory, skips it if it is hidden otherwise
		// makes sure it is created in the build dir
		if srcInfo.IsDir() {
			if srcInfo.Name()[0] == '.' {
				return filepath.SkipDir
			}
			if err := os.MkdirAll(dst, srcInfo.Mode().Perm()); err != nil {
				log.Fatalf("Can't create directory %s. %s\n", dst, err)
			}
			return nil
		}

		// skip special files (device, pipe, etc.)
		if !srcInfo.Mode().IsRegular() {
			return nil
		}

		// it is a normal file, copy it to build dir, but only if it is newer
		// or if force is enabled. Also skip hidden file
		if srcInfo.Name()[0] == '.' {
			return nil
		}
		mustCopy := force
		if !force {
			dstInfo, err := os.Stat(dst)
			if os.IsNotExist(err) {
				mustCopy = true
			} else {
				mustCopy = srcInfo.ModTime().After(dstInfo.ModTime())
			}
		}
		if mustCopy {
			contextUpdated = true
			in, err := os.Open(srcPath)
			if err != nil {
				log.Fatalf("Can't open %s. %s\n", srcPath, err)
			}
			defer in.Close()
			out, err := os.OpenFile(dst, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, srcInfo.Mode().Perm())
			if err != nil {
				log.Fatalf("Can't open %s. %s\n", dst, err)
			}
			defer out.Close()
			if _, err = io.Copy(out, in); err != nil {
				log.Fatalf("Can't copy '%s' to '%s'. %s\n", srcPath, dst, err)
			}
		}
		return nil
	}
	if err := filepath.Walk(srcDir, copyFile); err != nil {
		log.Fatalf("Service %s: Can't copy dockd context files. %s\n", serv.name, err)
	}
	if contextUpdated {
		fmt.Printf("Service %s: Dockd context files have been updated.\n", serv.name)
	} else {
		fmt.Printf("Service %s: Dockd context files are up to date.\n", serv.name)
	}
}

// generate a dev Dockerfile from an original. If forceBuild is set, the dev
// file is regenerated. If the original file is newer than the current dev file,
// it is also regenerated. The dev file is called Dockerfile.dockd.
//
// If the dev file has been updated, generated is returned. If it has been left
// untouched, uptodate is returned. If the Dockerfile uses an image dockd has no
// dev environment for, unknownImage is returned.
func generateDevDockerfile(tmpls *template.Template, serv service, forceBuild bool) genStatus {
	origPath := path.Join(serv.build, serv.dockerfile)
	dockdPath := path.Join(serv.build, dockdEnvDir)
	if err := os.MkdirAll(dockdPath, 0700); err != nil {
		log.Fatalf("Can't create dockd dir '%s'. %s\n", dockdPath, err)
	}
	dockdPath = path.Join(dockdPath, "Dockerfile")

	// check if the dev dockerfile has to be generated
	if !forceBuild {
		origStat, err := os.Stat(origPath)
		if err != nil {
			log.Fatalf("Can't read %s. %s\n", origPath, err)
		}
		dockdStat, err := os.Stat(dockdPath)
		switch {
		case err == nil:
			forceBuild = origStat.ModTime().After(dockdStat.ModTime())
		case os.IsNotExist(err):
			forceBuild = true
		default:
			log.Fatalf("Can't read %s. %s\n", dockdPath, err)
		}
	}

	// read the original dockerfile and check if we can handle the image
	dfBytes, err := ioutil.ReadFile(origPath)
	if err != nil {
		log.Fatalf("Can't read %s. %s\n", dockdPath, err)
	}
	re := regexp.MustCompile(`(?mi)^FROM\s+(\S+):(\S+)`)
	matches := re.FindSubmatch(dfBytes)
	if matches == nil {
		fmt.Printf("Service %s: Unknown docker image.\n", serv.name)
		return unknownImage
	}
	tmplData := &tmplData{
		Repo:     string(matches[1]),
		Tag:      string(matches[2]),
		FullName: fmt.Sprintf("%s:%s", matches[1], matches[2]),
	}
	tmpl := tmpls.Lookup(tmplData.Repo)
	if tmpl == nil {
		fmt.Printf("Service %s: Unknown docker image %s.\n", serv.name, tmplData.Repo)
		return unknownImage
	}

	// copy dockd context files to the docker context
	copyContextFiles(serv, forceBuild)

	// if the dev dockerfile is uptodate, stop here
	if !forceBuild {
		fmt.Printf("Service %s: Dockd dockerfile is up-to-date.\n", serv.name)
		return uptodate
	}

	// creates the dev Dockerfile
	output, err := os.Create(dockdPath)
	if err != nil {
		log.Fatalf("Can't write %s. %s\n", dockdPath, err)
	}
	fmt.Printf("Service %s: Generating dockd Dockerfile.\n", dockdPath)
	output.WriteString("# DO NOT EDIT.\n" +
		"# This file was automatically generated by dockd\n\n")
	buffer := bytes.NewBuffer(dfBytes)
	for {
		line, err := buffer.ReadString('\n')
		output.WriteString(line)
		if err != nil {
			if err == io.EOF {
				return generated
			}
			log.Fatalf("Can't read %s. %s\n", origPath, err)
		}
		if len(line) >= 4 && line[0:4] == "FROM" {
			tmpl.Execute(output, tmplData)
		}
	}
}

// returns the environment variables needed to run docker in docker-machine
func getDockerComposeEnv(machine, project string) []string {
	env := make([]string, 0, 8)
	dmEnv := execCapture("docker-machine", nil, "env", machine)
	dmEnv = strings.Replace(dmEnv, `"`, "", -1)
	for _, raw := range strings.Split(dmEnv, "export ") {
		if len(raw) > 0 {
			env = append(env, strings.SplitN(raw, "\n", 2)[0])
		}
	}
	env = append(env, fmt.Sprintf("COMPOSE_PROJECT_NAME=%s", project))
	return env
}

// execute the given docker-compose subcommand using the given environment.
// Input is used has stdin, it must be a yaml configuration.
func execDockerCompose(subcmd string, env []string, input []byte) {
	cmd := exec.Command("docker-compose", "-f", "-", subcmd)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = bytes.NewReader(input)
	cmd.Env = env
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Can't execute docker-compose %s. %s\n", subcmd, err)
	}
}

// start the development environments defined in a docker-compose configuration
// file. If docker machine is not running, it is started.
func startCmd(c *cli.Context) {
	machineName := c.String("machine")
	ymlName := c.String("file")
	forceBuild := c.Bool("force-build")

	templates := parseImageTemplates()
	pName, conf, services := parseComposeConf(ymlName)

	newDockerfile := false
	for _, s := range services {
		status := generateDevDockerfile(templates, s, forceBuild)
		if status != unknownImage {
			if status == generated {
				newDockerfile = true
			}
			conf[s.name]["dockerfile"] = path.Join(dockdEnvDir, "Dockerfile")
		}
	}

	startDockerMachine(machineName)

	confBytes, err := yaml.Marshal(conf)
	if err != nil {
		log.Fatalf("Can't generate configuration. %s\n", err)
	}
	env := getDockerComposeEnv(machineName, pName)

	if newDockerfile || forceBuild {
		fmt.Println("Building docker images...")
		execDockerCompose("build", env, confBytes)
	}

	fmt.Println("Starting docker containers...")
	execDockerCompose("up", env, confBytes)
}

// stop any running containers.
func stopCmd(c *cli.Context) {
	machineName := c.String("machine")
	ymlName := c.String("file")
	pName, conf, _ := parseComposeConf(ymlName)
	confBytes, err := yaml.Marshal(conf)
	if err != nil {
		log.Fatalf("Can't generate configuration. %s\n", err)
	}
	env := getDockerComposeEnv(machineName, pName)
	execDockerCompose("stop", env, confBytes)
}

// list running containers.
func psCmd(c *cli.Context) {
	machineName := c.String("machine")
	ymlName := c.String("file")
	pName, conf, _ := parseComposeConf(ymlName)
	confBytes, err := yaml.Marshal(conf)
	if err != nil {
		log.Fatalf("Can't generate configuration. %s\n", err)
	}
	env := getDockerComposeEnv(machineName, pName)
	execDockerCompose("ps", env, confBytes)
}

// get the running container name for the given service
func getContainerName(machine, yaml, service string) (env []string, container string, srv service) {
	project, _, services := parseComposeConf(yaml)
	env = getDockerComposeEnv(machine, project)

	container = fmt.Sprintf("%s_%s_", project, service)
	container = execCapture("docker", env, "ps",
		"--filter", "name="+container,
		"--format", "{{.Names}}")
	container = strings.TrimSpace(container)
	if container == "" {
		log.Fatalf("Unknown service %s or environment not running", service)
	}
	for _, s := range services {
		if s.name == service {
			srv = s
		}
	}
	return
}

// execute a bash shell in the given service
func shellCmd(c *cli.Context) {
	machineName := c.String("machine")
	ymlName := c.String("file")
	args := c.Args()
	if args.Present() && len(args) != 1 {
		cli.ShowSubcommandHelp(c)
		os.Exit(1)
	}
	service := args.Get(0)

	env, container, _ := getContainerName(machineName, ymlName, service)
	execPassthrough("docker", env, "exec", "-it", container, "bash")
}

// copy files between the given host and service
func copyCmd(c *cli.Context) {
	machineName := c.String("machine")
	ymlName := c.String("file")
	args := c.Args()
	if args.Present() && len(args) > 2 {
		cli.ShowSubcommandHelp(c)
		os.Exit(1)
	}

	cpArgs := make([]string, 2)
	var env []string
	foundContainer := false
	for i, arg := range args {
		parts := strings.SplitN(arg, ":", 2)
		if len(parts) == 2 {
			var container string
			var service service
			env, container, service = getContainerName(machineName, ymlName, parts[0])
			containerPath := parts[1]
			// if only one argument was provided, use the same path for both
			// local and remote.
			if len(args) == 1 {
				localPath := path.Join(service.build, containerPath)
				if strings.HasSuffix(containerPath, "/") {
					localPath = path.Join(localPath, "..")
				}
				cpArgs[1] = localPath
			}
			// convert relative to path to absolute
			if !path.IsAbs(containerPath) {
				cwd := execCapture("docker", env, "exec", container, "pwd")
				// paths ending with . have a special meaning with docker cp,
				// so we can't use path.Join since it cleans the resulting
				// path and remove the trailing .
				containerPath = fmt.Sprintf("%s/%s", strings.TrimSpace(cwd), containerPath)
			}
			cpArgs[i] = fmt.Sprintf("%s:%s", container, containerPath)
			foundContainer = true
		} else {
			cpArgs[i] = arg
		}
	}
	if env == nil || !foundContainer {
		cli.ShowSubcommandHelp(c)
		os.Exit(1)
	}

	execPassthrough("docker", env, "cp", cpArgs[0], cpArgs[1])
}

func main() {
	// make sure the dockd user dirs are created.
	for _, f := range []string{imagesDir, contextsDir} {
		path := os.ExpandEnv(f)
		if err := os.MkdirAll(path, 0700); err != nil {
			log.Fatalf("Can't create dockd user directories '%s'. %s\n", path, err)
		}
	}

	// init the application
	app := cli.NewApp()
	app.Name = "dockd"
	app.Usage = "A tool to manage dockerized development environments"
	app.Version = "1.2.0"
	app.Copyright = "Copyright © 2015, Florian Steffen Services"
	app.Authors = []cli.Author{{Name: "Florian Steffen", Email: "florian@steffen.services"}}
	app.Commands = []cli.Command{
		{
			Name:  "start",
			Usage: "Start the development environments",
			Description: `docker-machine is used to start a docker host for the development environments.
   If the machine is not running, it will be started.

   docker-compose is used to start all services specified in a docker-compose.yml 
   file. Each service is augmented using templates and assets defined in 
   $HOME/.dockd.`,
			Flags: []cli.Flag{
				cli.StringFlag{Name: "machine, m", Value: "default", Usage: "Name of the docker-machine used to run the dockd environment"},
				cli.StringFlag{Name: "file, f", Value: "docker-compose.yml", Usage: "Name of the docker-compose configuration file"},
				cli.BoolFlag{Name: "force-build", Usage: "Force the rebuilding of the underlying docker images"},
			},
			Action: startCmd,
		},
		{
			Name:    "stop",
			Usage:   "Stop the development environments",
			Flags: []cli.Flag{
				cli.StringFlag{Name: "machine, m", Value: "default", Usage: "Name of the docker-machine used to run the dockd environment"},
				cli.StringFlag{Name: "file, f", Value: "docker-compose.yml", Usage: "Name of the docker-compose configuration file"},
			},
			Action: stopCmd,
		},
		{
			Name:    "process",
			Aliases: []string{"ps"},
			Usage:   "List running development environments",
			Flags: []cli.Flag{
				cli.StringFlag{Name: "machine, m", Value: "default", Usage: "Name of the docker-machine used to run the dockd environment"},
				cli.StringFlag{Name: "file, f", Value: "docker-compose.yml", Usage: "Name of the docker-compose configuration file"},
			},
			Action: psCmd,
		},
		{
			Name:    "shell",
			Aliases: []string{"sh"},
			Usage:   "Enter a shell inside the given service",
			Description: `Run a bash shell inside the docker container of the specified service.
   The development environments must be started before this command can be ran.`,
			Flags: []cli.Flag{
				cli.StringFlag{Name: "machine, m", Value: "default", Usage: "Name of the docker-machine used to run the dockd environment"},
				cli.StringFlag{Name: "file, f", Value: "docker-compose.yml", Usage: "Name of the docker-compose configuration file"},
			},
			Action: shellCmd,
		},
		{
			Name:    "copy",
			Aliases: []string{"cp"},
			Usage:   "Copy files between the host and the given service",
			Description: `If arguments are <service>:<path> <path> or <service>:<path>, copy
   files from the running service to the host. If only a single argument
   is provided, the same path is used for both the host and the container.
   It can be used to quickly copy the changes made in the container back
   to the host.
   With a single argument, if the path ends with a slash, it is interpreted 
   as a directory. To allow container to host synchronisation, the container 
   directory is copied in the parent directory on the host machine.

   If arguments are <path> <service>:<path>, copy files from the host to 
   the running service.

   If the service path is relative, it is interpreted as relative to the
   working directory in the container.`,
			Flags: []cli.Flag{
				cli.StringFlag{Name: "machine, m", Value: "default", Usage: "Name of the docker-machine used to run the dockd environment"},
				cli.StringFlag{Name: "file, f", Value: "docker-compose.yml", Usage: "Name of the docker-compose configuration file"},
			},
			Action: copyCmd,
		},
	}
	app.Run(os.Args)
}
