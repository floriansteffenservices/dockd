FROM golang:1.5

RUN go get -u github.com/codegangsta/cli \
    gopkg.in/yaml.v2

COPY . /go/src/bitbucket.org/floriansteffenservices/dockd
WORKDIR /go/src/bitbucket.org/floriansteffenservices/dockd

ENV GOOS=darwin
ENV GOARCH=amd64

CMD go build -v
