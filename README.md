# dockd #

`dockd` is a tool to run dockerized development environments from a production docker-compose config file. It generates a modified config file using user defined templates. One can add development tools for a particular language that way. `dockd` provides a command to quickly enter a shell in the container (e.g. to run development tools) and to copy files between the container and the host machine.

## Requirements ##

`dockd` uses various docker related tools, so `docker`, `docker-compose` and `docker-machine` must be installed and configured on your machine. On Windows and Mac OS X, simply use [Docker Toolbox](https://github.com/docker/toolbox/releases/) for this. On linux, follow the instructions provided by Docker to install the correct packages for your distribution. 

## Installation ##

Clone or download the Git repository hosted on [BitBucket](https://bitbucket.org/floriansteffenservices/dockd)

Then run the following commands to generate a dockd executable:

    docker build -t dockd .
    docker run --rm -v `pwd`:/go/src/bitbucket.org/floriansteffenservices/dockd dockd

You should have a `dockd` executable in your working directory.

The default configuration produces a 64bits executable for Mac OS X. This can be changed by adding -e GOOS=... and -e GOARCH=... to the docker run command above.

When you are done, you can remove the docker image with the following command:

    docker rmi dockd

## Usage ##

    dockd [global options] command [command options] [arguments...]

Available commands:

    start          Start development environments
    stop           Stop the development environments
    process, ps    List running development environments
    shell, sh      Enter a shell inside the given service
    copy, cp       Copy files between the host and the given service
    help, h        Shows a list of commands or help for one command
   
Global options:

    --help, -h          show help
    --version, -v       print the version

### Start ###

Start the development environments.

    dockd start [command options]

`docker-machine` is used to start a docker host for the development environments.
If the machine is not already running, it will be started.

`docker-compose` is used to start all services specified in a `docker-compose.yml` 
file. Each service is augmented using templates and assets defined in `$HOME/.dockd`.

Command options:

    --machine, -m "default"             Name of the docker-machine used to run the 
                                        dockd environment
    --file, -f "docker-compose.yml"     Name of the docker-compose configuration file
    --force-build                       Force the rebuilding of the underlying docker
                                        images

### Stop ###

Stop the development environments.

    dockd stop [command options] [arguments...]

Command options:

    --machine, -m "default"            Name of the docker-machine used to run the dockd environment
    --file, -f "docker-compose.yml"    Name of the docker-compose configuration file


### Process ###

List running development environments.

    dockd process [command options] [arguments...]

Command options:

    --machine, -m "default"            Name of the docker-machine used to run the dockd environment
    --file, -f "docker-compose.yml"    Name of the docker-compose configuration file


### Shell ###

Enter a shell inside the given service.

    dockd shell [command options] [arguments...]

Run a bash shell inside the docker container of the specified service. The development environments must be started before this command can be ran.

Command options

    --machine, -m "default"             Name of the docker-machine used to run the dockd
                                        environment
    --file, -f "docker-compose.yml"     Name of the docker-compose configuration file

### Copy ###

Copy files between the host and the given service running in a container.

    dockd copy [command options] [arguments...]

If arguments are <service>:<path> <path> or <service>:<path>, copy files from the running service to the host. If only a single argument is provided, the same path is used for both the host and the container. It can be used to quickly copy the changes made in the container back to the host. With a single argument, if the path ends with a slash, it is interpreted as a directory. To allow container to host synchronisation, the container directory is copied in the parent directory on the host.

If arguments are <path> <service>:<path>, copy files from the host the running service.

If the service path is relative, it is interpreted as relative to the working directory in the container.

Command options:

    --machine, -m "default"             Name of the docker-machine used to run the dockd 
                                        environment
    --file, -f "docker-compose.yml"     Name of the docker-compose configuration file

## Files ##

`dockd` is using various files stored in the `$HOME/.dockd` directory.

### Templates ###

Templates must be stored in the `$HOME/.dockd/images/` directory. When `dockd` find a local image (i.e. a `build` clause) in a `docker-compose.yml`, it will attempt to find a template with the same name. If it is available, the template is executed and its content is inserted just after the `FROM` clause of the original `Dockerfile`.

Templates are using the [Go's template syntax](https://golang.org/pkg/text/template/).

`dockd` is loading all templates present in the `$HOME/.dockd/images/` directory, so it is possible to define subtemplates and include them from the top-level (image) templates.

The following data are available to the templates:

  - `.Repo` the name of the image (e.g. golang)
  - `.Tag` the tag of the image (e.g. 1.5-onbuild)
  - `.FullName` the name and tag of the image (e.g. golang:1.5-onbuild)

### Assets ###

Files in `$HOME/.dockd/contexts/` are added to the docker context and so can be referenced from the templates. It is good practice to create a subdirectory per image template, but it is not mandatory. The assets are copied in a `.dockd` directory in the docker context. For example if `README` was added in `$HOME/.dockd/contexts/python`, it can be accessed from a template like this: `COPY .dockd/python/README /code/`.

Two helper functions are available to handle assets:

  - `copy`, to copy an asset from the host to the container. It behave like the `COPY` command of Docker, except it takes care of the assets location. The previous example would become `{{copy "python/README" "/code/"}}`.
  - `append`, to append an asset to an existing file in the container. For example `{{append "python/ADDENDUM" "/code/README"}}` would append the content of `ADDENDUM` to the container's `README` file.

## Example ##

We will use a simple [django](http://djangoproject.com) project to demonstrate how `dockd` can be used. It follows the example given in the `docker-compose` documentation.

First defines a `Dockerfile` for your django application:

```
#!Dockerfile
FROM python:2.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
```

Then defines a `requirements.txt` for the python dependencies:

```
Django
pdycopg2
```

Now you can define all services needed by the project in a `docker-compose.yml` file:

```
#!yaml
db:
  image: postgres
web:
  build: .
  command: python manage.py runserver 0.0.0.0:8000
  ports:
    - "8000:8000"
  links:
    - db
```

Suppose you want to add Vim to your environment to edit your project. Add a template for the python image in a file named `$HOME/.dockd/images/python.tmpl`:

```
#!Dockerfile
{{define "python"}}
RUN apt-get update \
    && apt-get install -y vim \
    && rm -rf /var/lib/apt/lists/*
{{end}}
```

Now you can run your project with the command: 

    dockd start

The following happens:

 1. Starts a virtual machine (or any host supported by docker-machine) to run the two docker containers. Basically, `docker-machine start <machine>` is executed if <machine> is not already running.
 2. Parses your `docker-compose.yml` file, and creates a modified `Dockerfile` using the `python.tmpl` template.
 3. Builds the docker images for the project by running `docker-compose build`.
 4. Runs the docker containers for the project by executing `docker-compose up`.

In this case, the generated `Dockerfile` is:

```
#!Dockerfile
FROM python:2.7
RUN apt-get update \
    && apt-get install -y vim \
    && rm -rf /var/lib/apt/lists/*
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
```

The command takes care of setting the environment variables to run the various docker tools and to rebuild the images when the configuration files are changed.

In a second shell, you can now enter the python environment with the command:

    dockd shell web

This will execute a bash shell inside the running container for the service `web`. You can run `vi` to edit your project, or run `django-admin.py` to perform various django tasks. Once done, quit the container (`<ctrl-d>` or `exit`).

If you modify some files in the container, you can copy them back to your host with the command:

    dockd cp web:./

This will execute a `docker cp` in the working directory of the container for the service `web`, and copies every files on your host. You can also use volumes for this part, but they are really slow when run in a virtual machine like boot2docker with VirtualBox or VMware.

## License ##

The project is licensed under the term of the Modified BSD License. See LICENSE file for more details.
